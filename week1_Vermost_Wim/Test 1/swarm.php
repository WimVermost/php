<?php

    $posts = [   
        ["userpicture" => "https://s3.amazonaws.com/uifaces/faces/twitter/idiot/128.jpg", 
         "name" => "Bart Meeuwvis", 
         "place" => "Times Square",
         "postimage" => "http://www.nycgo.com/images/460x285/TimesSquareSummer_460x285.jpg",
         "city" => "New York",
         "tijd" => "22m",
         "update" => "Even chillen in New York"],
        
        ["userpicture" => "https://s3.amazonaws.com/uifaces/faces/twitter/idiot/128.jpg",
         "name" =>"Bart Meeuwvis", 
         "place" => "Veemarkt",
         "postimage" => "http://mechelen.mapt.be/images/thumb/7/71/VeemarktA.jpg/400px-VeemarktA.jpg",
         "city" => "Mechelen",
          "tijd" => "39m",
         "update" =>"Nu ben ik in mechelen"],
        
        ["userpicture" =>"http://i0.kym-cdn.com/entries/icons/original/000/003/047/omg.jpg", 
         "name" =>"Wim Vermost", 
         "place" => "Groenplaats",
         "postimage" => "",
         "city" => "Antwerpen",
          "tijd" => "49m",
         "update" =>"Ik ben in Antwerpen"]      
    ];
    
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Swarm</title>
</head>
<body>
    
    <section class="timeline">
        <?php
        
        foreach( $posts as $post ): ?>
        <article>
            <img class="profilepic" src="<?php echo $post['userpicture'] ?>" alt="">
            <h3><?php echo $post['name'] ?></h3>
            <h4><?php echo $post['place'] ?></h3>
            <div class="update"><?php echo $post['city'] ?></div>
            <?php 
                
            if( !empty($post['postimage'])){
               ?>
               <img class="placepic" src="<?php echo $post['postimage'] ?>" alt=""> <?php
            }
            ?>
        </article>
        <?php endforeach; ?>
    </section>
    

    

<style>
    body{
        font-family: Helvetica;
        
    }
    .timeline{
       
        width: 50%;
        margin-left: 25%;
        margin-right: 25px;
    }
    .profilepic{
        border-radius: 100%;
        float: left;
        margin-right: 40px;
        width: 50px;
    }    
    h3{
        color: #00ABDA;
        font-size: 1.2em;
        margin-top: 20px;
        margin-bottom: -20px;
    }
    h4{
         color: #00ABDA
       
    }
    .update{
        margin-top: -10px;
        margin-left: 90px;
        margin-bottom: 50px;
    }
    article{
        
        border-bottom: 1px solid gray;
    }
    .placepic{
        margin-left: 90px;
        width: 200px;
        margin-top: -30px;
        margin-bottom: 20px;
    }
</style>        
</body>
</html>