<?php

    $todolist = [   
        ["beschrijving" => "Beschrijving van de todo", 
         "urenwerk" => 2, 
         "categorie" => "Thuis",],
        
         ["beschrijving" => "Zware todo", 
         "urenwerk" => 5, 
         "categorie" => "School",],
        
         ["beschrijving" => "Toch vrij zwaar", 
         "urenwerk" => 3, 
         "categorie" => "Werk",],
        
         ["beschrijving" => "Enorm veel werk", 
         "urenwerk" => 10, 
         "categorie" => "Thuis",],
        
         ["beschrijving" => "Niet teveel werk", 
         "urenwerk" => 1, 
         "categorie" => "School",],
        
         ["beschrijving" => "Hier ga ik lang aan zitten", 
         "urenwerk" => 8, 
         "categorie" => "School",],
        
         ["beschrijving" => "Ook vrij lang", 
         "urenwerk" => 4, 
         "categorie" => "Thuis",],
        
         ["beschrijving" => "Snel gedaan", 
         "urenwerk" => 0.2, 
         "categorie" => "Thuis",],
    ];

function cmp($a, $b) {
        return $b["urenwerk"] - $a["urenwerk"];
        // a & b omdraaien rangschikt ze omgekeerd.
}
usort($todolist, "cmp");



?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>To do list</title>
</head>
<body>
    <h2>To do list</h2>
    <div id="container">
        <?php
        
        foreach( $todolist as $post ): ?>
       
           <section <?php 
            
            if($post['urenwerk'] >= 5){ 
                echo'class="red"';
            } else if($post['urenwerk'] >= 1){
                echo'class="orange"';
            } else{ 
                echo'class="green"'; 
            } ?>>
               
            <span class="beschrijving"> <?php echo $post['beschrijving'] ?>   </span>
            
            <?php if($post['categorie'] == "Thuis"){
                    ?> <img src="img/home-page.png" alt="png van een huis">
            <?php } else if($post['categorie'] == "School"){
                      ?> <img src="img/college-graduation.png" alt="png van school"> <?php       
                    } else{
                     ?> <img src="img/businessman-working-online-with-a-laptop.png" alt="png van werk"> <?php  
            }
             ?>
           </section>
           
       
        <?php endforeach; ?>
    
    </div>
       
</body>
<style>
    body{
        color: white;
        font-family: arial;
    }
    img{
        width: 20px;
        height: 20px;
        float: right;
        margin-right: 10px;
        margin-top: 15px;
    }
    .beschrijving{
        line-height: 50px;
        margin-left: 10px;
    }
  
    .categorie{
         line-height: 50px;
        float: right; 
        margin-right: 10px;
    }
    h2{
        text-align: center;
        width: 100%;
    }
    #container{
        margin-left: auto;
        margin-right: auto;
        display: block;
        width: 300px; 
    }
    section{
        width: 300px;
        display: block;
        height: 50px;
        border: 1px solid white;
        border-top: 0px;
    } 
    .red{
        background-color: #E1614C;
    }
    .red:hover{
        background-color: #CA472E;
    }
    .orange{
       background-color: #EBCC1D; 
    }
    .orange:hover{
        background-color: #D3B400;
    }
    .green{
        background-color: #2BCF91;
    }
    .green:hover{
        background-color: #009D5C;
    }
</style>
</html>