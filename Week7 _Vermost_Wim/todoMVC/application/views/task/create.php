<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Add a task</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1>Add a new task</h1>
				<form action="" method="POST">
				<?php if ( !empty($feedback)): ?>
				<div class="alert alert-success"><?php echo $feedback ?></div>
				<?php endif; ?>
				
				<?php echo validation_errors(); ?>
					<div class="form-group">
						<label for="todo_name">Todo name</label>
						<input type="text" class="form-control" name="todo_name" placeholder="Todo">
					</div>
					<button type="submit" class="btn btn-primary">Add task</button>
				</form>
				<a href="all"><button class="btn btn-default" style="margin-top: 10px">View todo's</button></a>
			</div>		
		</div>
	</div>
	
</body>
</html>