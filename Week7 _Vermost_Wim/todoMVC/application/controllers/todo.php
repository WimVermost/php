<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Todo extends CI_Controller {
	
	public function create()
	{
		
		$viewdata['feedback'] = "";
		// validate form
		$this->load->library('form_validation');
		$this->form_validation->set_rules('todo_name', 'Task name', 'required');
		if ($this->form_validation->run() === FALSE){
			// validation failed
		}else{
			// validation ok
			// save via model
			$this->load->model('Task_model');
			$data['name'] = $this->input->post('todo_name');
			$this->Task_model->Save($data);
			$viewdata['feedback'] = "Your todo has been added.";
		}
		$this->load->view('task/create', $viewdata);
	}

	public function all(){
		$data = [];
		$this->load->model('Task_model');
		$data['todos'] = $this->Task_model->getAll();
		$this->load->view('task/all', $data);
	}
}
