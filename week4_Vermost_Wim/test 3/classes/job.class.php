<?php
include_once("db.class.php");

    class Job
    {
        private $m_sJobTitle;
        private $m_sFromUntil;
        private $m_sJobDescription;

        public function __set($p_sProperty, $p_vValue)
        {
            switch ($p_sProperty) {
                case "JobTitle":
                    if (!empty($p_vValue)) {
                        $this->m_sJobTitle = $p_vValue;
                    } else {
                        throw new Exception("Jobtitle cannot be empty");
                    }
                    break;
                case "FromUntil":
                    if (!empty($p_vValue)) {
                        $this->m_sFromUntil = $p_vValue;
                    } else {
                        throw new Exception("Field from-until cannot be empty");
                    }
                case "JobDescription":
                    if (!empty($p_vValue)) {
                        $this->m_sJobDescription = $p_vValue;
                    } else {
                        throw new Exception("Job Description cannot be empty");
                    }
            }
        }

        public function __get($p_sProperty)
        {
            switch ($p_sProperty) {
                case "JobTitle":
                    return $this->m_sJobTitle;
                    break;
                case "FromUntil":
                    return $this->m_sFromUntil;
                    break;
                case "JobDescription":
                    return $this->m_sJobDescription;
                    break;
            }

        }

        public function Save(){
            $conn = Db::getInstance();
            $statement = $conn->prepare("INSERT INTO `jobs`(`title`, `fromuntil`, `description`)
            values(:jobtitle, :fromuntil, :jobdescription)");
            $statement->bindValue(":jobtitle", $this->m_sJobTitle);
            $statement->bindValue(":fromuntil", $this->m_sFromUntil);
            $statement->bindValue(":jobdescription", $this->m_sJobDescription);

            return $statement->execute();
        }

        public function getAll(){
            $conn = Db::getInstance();
            $results = $conn->query("select * from jobs ORDER BY 'id' DESC");
            return $results;
        }

    }
?>