<?php
	include_once "classes/job.class.php";

		if(isset($_POST['add'])){
			$job = new Job();
			$job->JobTitle = $_POST['title'];
			$job->FromUntil = $_POST['time'];
			$job->JobDescription = $_POST['description'];
			$job->Save();
			header("Refresh:0");
		}
		$timeline = new Job();
		$posts = $timeline->getAll();

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Timeline</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
</head>
<body>
<!-- The Timeline -->
<form action="" method="POST">
<h3>Add job</h3>
	<label for="title">Job title:</label>
	<input type="text" name="title" id="title">
	<label for="time">During:</label>
	<input type="text" name="time" id="time">
	<label for="description">Job description:</label>
	<input type="text" name="description" id="description">
	<input type="submit" class="btn" name="add" value="Add job">
</form>
<ul class="timeline">
	<?php
	while($p = $posts->fetch(PDO::FETCH_ASSOC)): ?>
		<li><?php if( ($p['id'] % 2) == 0){?>
			<div class="direction-r"><?php }
				else { ?>
				<div class="direction-l"><?php } ?>
				<div class="flag-wrapper">
					<span class="flag"><?php echo htmlspecialchars($p['title']);?></span>
					<span class="time-wrapper"><span class="time"><?php echo htmlspecialchars($p['fromuntil']); ?></span></span>
				</div>
				<div class="desc"><?php echo htmlspecialchars($p['description']);?></div>
			</div>
		</li>
	<?php endwhile; ?>
</ul>
</body>
</html>