<?php

    include_once("Vehicle.class.php");
    class Car extends Vehicle{

        public function Save(){

            $conn = new PDO('mysql:host=localhost; dbname=imd', 'root', 'root');
            $statement = $conn->prepare("insert into cars (brand, price) values (:brand, :price)");
            $statement->bindValue(":brand", $this->Brand);
            $statement->bindValue(":price", $this->Price);
            $result = $statement->execute();
            return $result;
            // return true or false
        }
        public function __toString()
        {
            // echo van het object doet dit. bvb echo $car;
            $vResult = "<h1>" . $this->Brand ." " . $this->Price . "</h1>";
            return $vResult;
        }
    }

?>