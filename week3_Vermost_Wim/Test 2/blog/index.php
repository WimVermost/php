<?php
    
    ob_start();
    $t=time();

    $conn = new PDO('mysql:host=localhost; dbname=imd', 'root', 'root');

    $posts = $conn->query("select * from posts ORDER BY `id` DESC LIMIT 3");

    if( isset($_POST['place']) ){
        $title = $_POST['title'];
        $post = $_POST['post'];
        $date = date("Y-m-d",$t);
        $hour = date("h:i:sa",$t);

        $statement = $conn->prepare("insert into posts (title, post, date, hour) values (:title, :post, :date, :hour)");
        $statement->bindParam(":title", $title);
        $statement->bindParam(":post", $post);
        $statement->bindParam(":date", $date);
        $statement->bindParam(":hour", $hour);
        
        $statement->execute();
        header("Refresh:0");
    
    }
    if(isset($_POST['delete'])){
          $query = "delete from posts";
          $statement = $conn->prepare($query);
          $statement->execute();
          header("Refresh:0");
    }
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
<?php while($p = $posts->fetch(PDO::FETCH_ASSOC)): ?>
<article>
    <h2><?php echo $p['title']; ?></h2>
    <div class="post"><?php echo htmlspecialchars($p['post']); ?></div>
    <span>Posted on: <?php echo $p['date']; ?></span>
    <span>Hour: <?php echo $p['hour']; ?></span>
</article>
<?php endwhile; ?>

    <form action="" method="post">
        <div>
            <h3>Post:</h3>
            <label for="title">Title</label>
            <input type="text" name="title" class="form-control" placeholder="Title">
        </div>
        <div>
            <label for="post">Post</label>
            <textarea class="form-control"  name="post" id="post" cols="30" rows="5" placeholder="Blogpost"></textarea>
        </div>
        <div id="buttons">
             <input type="submit" value="Place" name="place" class="btn btn-primary">
            <input type="submit" name="delete" value="Erase All" class="btn btn-danger">
        </div>
               
    </form>
    <style>
        article, div{
            width: 400px;
            margin-left: auto;
            margin-right: auto;
            text-align: left;   
        }
        article{
            text-transform: capitalize;
            margin-bottom: 10px;
            border-bottom: 1px solid #CCCCCC;
        }
        #buttons{
            display: flex;
            justify-content: space-between;
            margin-bottom: 20px;
        }
        div input, textarea{
             width: 100%;
             border: 1px solid #CCCCCC;
        }
        div textarea{
            height: 50px;
        }
        .btn{
            width: 45%;
            margin-top: 5px;
            border-radius: 0;
        }
        .post{
            padding-top: 10px;
            padding-bottom: 10px;
        }
        span{
            color: gray;
            font-size: 0.7em;
            display: block;
            width: 100%;
        }
    </style>
</body>
</html>