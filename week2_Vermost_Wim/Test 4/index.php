<?php


$GLOBALS['username'] ="";
$GLOBALS['password'] = "";

function canLogin( $p_username, $p_password ){
    
        if($p_username == $GLOBALS['username'] && $p_password == $GLOBALS['password']){
            return true;   
        }
        else{
            return false;
        }      
}

if(isset( $_POST['btnLogin'])){
        
        if(canLogin($username, $password)){
            session_start();
            $_SESSION['loggedin'] = "yes"; 
            $_SESSION['user'] = $_POST['username'];
            
        }
}

if(isset ($_POST['btnSignup'])){
     
   $GLOBALS['username'] = $_POST['s_email'];
   $GLOBALS['password'] = $_POST['s_password'];
}

?><!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>IMD Talks</title>
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/twitter.css">
	
</head>
<body>
	<nav>
		<?php if(isset($_SESSION['loggedin'])): ?>
			<a href="logout.php">Logout</a>
		<?php else: ?>
			<a href="index.php">Login</a>
		<?php endif; ?>
	</nav>

	<header>
		<h1>Welcome to IMD-Talks</h1>
		<h2>Find out what other IMD'ers are building around you.</h2>
	</header>
	
	
	<div id="rightside">	
	<?php if(!isset($_SESSION['loggedin'])): ?>
	<section id="login">
		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" autocomplete="false">
		<input type="text" name="username" placeholder="Email" />
		<input type="password" name="password" placeholder="Password" />
		<input type="checkbox" name="rememberme" value="yes" id="rememberme">
		<label for="rememberme">Remember me</label>

		<input type="submit" method="post" name="btnLogin" value="Sign in" />
		</form>
		
	</section>	
	<?php endif; ?>
	<section id="signup">
	<?php if(isset($_SESSION['loggedin'])): ?>
		<h2>Welcome <?php echo $_SESSION['user']; ?></h2>
		
		<?php else: ?>
		<h2>New to IMD-Talks? <span>Sign Up</span></h2>
		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" autocomplete="false">
		<input type="text" name="s_name" placeholder="Full name" />
		<input type="email" name="s_email" placeholder="Email" />
		<input type="password" name="s_password" placeholder="Password" />
		<input type="submit" name="btnSignup" value="Sign up for IMD Talks" />
		</form>
		<?php endif; ?>
	</section>
	</div>	
	
</body>
</html>