<?php
include_once('include_products.php');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Producten</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
   <section>
    <?php
    foreach( $products as $key=>$post):?>
       
        <ul>
            <li><h2> <?php echo $post['name']. str_repeat('&nbsp;', 4)."€" .$post['price'] ; ?></h2>
            <img src="<?php echo $post['image']; ?>" alt="">
            <a href="details.php?product=<?php echo $key; ?>">more info</a>
            </li>
            
        </ul>  
             
    <?php     
    endforeach; ?>
    </section>
    
</body>

</html>